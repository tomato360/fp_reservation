module DefaultHelper
  def datetime_to_hm(datetime)
    "#{datetime.strftime("%H:%M")}"
  end
end
