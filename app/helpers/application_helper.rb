module ApplicationHelper
  def datetime_to_ymdhm(datetime)
    datetime.strftime("%Y/%m/%d %H:%M")
  end
end
