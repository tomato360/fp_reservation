class RejectSundayValidator < ActiveModel::Validator
  def validate(record)
    if record.start_time.wday == 0
      record.errors[:start_time] << '日曜日は稼働日に指定できません'
    end
  end
end
