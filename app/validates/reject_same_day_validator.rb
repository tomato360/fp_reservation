class RejectSameDayValidator < ActiveModel::Validator
  def validate(record)
    if WorkingDay.where(user_id: record.user_id).find_by_start_time(record.start_time)
      record.errors[:start_time] << 'すでにその稼働日は登録されています'
    end
  end
end
