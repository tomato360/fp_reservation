class WorkingDaysController < ApplicationController
  before_action :session_check
  before_action :login_required
  before_action :fp_user_only

  def index
    @working_days = WorkingDay.page(params[:page]).where(user_id: @current_user.id).order('start_time DESC')
  end

  def new
    @working_day = WorkingDay.new
  end

  def create
    @working_day = WorkingDay.new params_working_day(params)
    @working_day.user_id = @current_user.id
    if @working_day.save
      redirect_to root_path, flash: { success: "新しく稼働日を#{@working_day.start_time}に設定しました。" }
    else
      flash.now[:danger] = "#{@working_day.start_time}は新しい稼働日に設定できませんでした。"
      render action: :new
    end
  end

  def edit
  end

  def destroy
    working_day = WorkingDay.find(params[:id]).destroy

    if working_day.frozen?
      redirect_to working_days_path, flash: { success: "#{working_day.start_time}を稼働日一覧から削除しました。" }
    else
      redirect_to working_days_path, flash: { danger: "#{working_day.start_time}を稼働日一覧から削除出来ませんでした。" }
    end
  end

  private

  def params_working_day(params)
    params.require(:working_day).permit([:start_time])
  end
end
