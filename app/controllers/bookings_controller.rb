class BookingsController < ApplicationController
  before_action :session_check
  before_action :login_required
  before_action :user_only

  def index
    @bookings = Booking.page(params[:page]).includes(working_day: :user).where(user_id: @current_user.id).order('working_days.start_time DESC')
  end

  def create
    booking = Booking.new(user_id: @current_user.id, working_day_id: params[:working_day_id])

    if booking.save
      redirect_to root_path, flash: { success: '予約しました' }
    else
      redirect_to root_path, flash: { danger: '予約が失敗しました'}
    end
  end

  def destroy
    booking = Booking.find(params[:id]).destroy

    if booking.frozen?
      redirect_to bookings_path, flash: { success: "#{booking.working_day.start_time}を予約一覧から削除しました。" }
    else
      redirect_to bookings_path, flash: { danger: "#{booking.working_day.start_time}を予約一覧から削除出来ませんでした。" }
    end
  end
end
