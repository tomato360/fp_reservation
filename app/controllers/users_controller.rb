class UsersController < ApplicationController
  before_action :session_check

  def new
    @user = User.new
  end

  def show
  end

  def create
    @user = User.new user_params params

    if @user.save
      redirect_to user_complete_path @user
    else
      render :new
    end
  end

  def complete
    session[:user_id] = params[:user_id]
    @user = User.find params[:user_id]
  end

  def destroy
    @current_user.destroy

    if @current_user.frozen?
      session[:user_id] = nil
      redirect_to root_path, flash: { success: "退会完了しました。" }
    else
      redirect_to user_path, flash: { danger: "退会できませんでした。" }
    end
  end

  private

  def user_params(params)
    params.require(:user).permit([:name, :role])
  end
end
