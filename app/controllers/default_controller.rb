class DefaultController < ApplicationController
  before_action :session_check

  def index
    case @current_user.try(:role)
    when 'user' then
      @working_days = WorkingDay.includes([:booking, :user]).all
    when 'fp_user' then
      @working_days = WorkingDay.includes([:booking, :user]).where(user_id: @current_user.id)
    end
  end
end
