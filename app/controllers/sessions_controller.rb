class SessionsController < ApplicationController

  def new
    @session = Session.new
  end

  def create
    @user = User.find_by_name params_name(params)
    if @user.presence
      session[:user_id] = @user.id
        redirect_to root_path
    else
      @session = Session.new
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to :root
  end

  private

  def params_name(params)
    params.require(:session).permit(:name)[:name]
  end
end
