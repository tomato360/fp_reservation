class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def session_check
    return @current_user if @current_user

    @current_user = User.find(session[:user_id]) if session[:user_id]
  end

  def login_required
    redirect_to :root unless @current_user
  end

  def fp_user_only
    redirect_to :root if @current_user.try(:role) == 'user'
  end

  def user_only
    redirect_to :root if @current_user.try(:role) == 'fp_user'
  end
end
