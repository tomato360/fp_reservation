class Session
  include ActiveModel::Validations
  include ActiveModel::Conversion

  attr_accessor :name

  validates :name, presence: true
end
