# == Schema Information
#
# Table name: working_days
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  start_time :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class WorkingDay < ActiveRecord::Base
  belongs_to :user
  has_one :booking, dependent: :destroy

  validates_with RejectSameDayValidator
  validates_with RejectSundayValidator
  validates_time :start_time, between: '10:00am'...'18:00pm', if: :weekday?
  validates_time :start_time, between: '11:00am'...'15:00pm', if: :saturday?

  paginates_per 10

  private

  def weekday?
    start_time.wday != 0 && start_time.wday != 6
  end

  def saturday?
    start_time.wday == 6
  end
end
