# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  role       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
  validates :role, presence: true

  has_many :working_days, dependent: :destroy
  has_many :bookings, dependent: :destroy

  enum role: {
    user: 0,
    fp_user: 1
  }
end
