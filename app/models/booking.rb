# == Schema Information
#
# Table name: bookings
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  working_day_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Booking < ActiveRecord::Base
  belongs_to :user
  belongs_to :working_day

  paginates_per 10
end
