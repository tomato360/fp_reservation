class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.belongs_to :users
      t.datetime :start_time
      t.timestamps null: false
    end
  end
end
