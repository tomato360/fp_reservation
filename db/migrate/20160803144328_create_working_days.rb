class CreateWorkingDays < ActiveRecord::Migration
  def change
    create_table :working_days do |t|
      t.belongs_to :user
      t.datetime :start_time
      t.timestamps null: false
    end
  end
end
