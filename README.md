# README

## セットアップ手順

MACの場合のみ記述する

1. アプリケーションを取得
  ```
  git clone https://tomato360@bitbucket.org/tomato360/fp_reservation.git
  ```

1. rubyの環境を構築

  ```
  brew install rbenv
  rbenv install 2.3.0
  rbenv local 2.3.0
  ```

1. mysqlのインストール

  ```
  bew install mysql
  ```

1. railsアプリケーションのセットアップ

  ```
  bundle install --path vendor/bundle
  ```

1. データベースのセットアップ

  ```
  bundle exec rake db:create
  bundle exec rake db:migrate
  ```

1. アプリケーションの起動

  ```
  bundle exec rails s
  ```
