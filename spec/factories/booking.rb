FactoryGirl.define do
  factory :booking do
    association :working_day, factory: :working_day
    association :user, factory: :user
  end
end
