FactoryGirl.define do
  factory :user, class: User do
    name Faker::Name.name
    role 'user'
  end

  factory :fp_user, class: User do
    name Faker::Name.name
    role 'fp_user'

    trait :working_day do
      after(:create) do |user|
        create(:working_day, user: user)
      end
    end
  end
end
