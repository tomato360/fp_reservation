FactoryGirl.define do
  factory :working_day, class: WorkingDay do

    association :user, factory: :fp_user

    start_time '2016-08-08 13:00:00'

    trait :saturday do
      start_time '2016-08-06 13:00:00'
    end

    trait :saturday_before_hour do
      start_time '2016-08-06 10:00:00'
    end

    trait :saturday_after_hour do
      start_time '2016-08-06 15:30:00'
    end

    trait :sunday do
      start_time '2016-08-07 13:00:00'
    end

    trait :with_booking do
      after(:create) do |working_day|
        create(:booking, working_day: working_day)
      end
    end
  end
end
