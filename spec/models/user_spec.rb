# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  role       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#create' do
    context 'nameのみ入力されているとき' do
      subject { User.create(name: Faker::Name.name ) }
      it 'バリデーションが通らないこと' do
        is_expected.not_to be_valid
      end
    end

    context 'roleのみ入力されているとき' do
      subject { User.create(role: 'user' ) }
      it 'バリデーションが通らないこと' do
        is_expected.not_to be_valid
      end
    end
  end

  describe '#destroy' do
    let(:user) { create(:fp_user, :working_day) }

    it 'working_dayも削除されること' do
      expect{ user.destroy }.to change{ user.working_days.size }.from(1).to(0)
    end
  end
end
