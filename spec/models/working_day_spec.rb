# == Schema Information
#
# Table name: working_days
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  start_time :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe WorkingDay, type: :model do
  describe '#craete' do
    context '平日の時' do
      let(:working_day) { create(:working_day) }
      it "保存されること" do
        expect{ working_day }.to change{ WorkingDay.all.size }.by(1)
      end
    end

    context '土曜日の時' do
      context '10時〜15時の間で' do
        let(:working_day) { create(:working_day, :saturday) }
        it '保存されること' do
          expect{ working_day }.to change{ WorkingDay.all.size }.by(1)
        end
      end

      context '10時より前では' do
        subject { build(:working_day, :saturday_before_hour) }
        it 'バリデーションエラーになること' do
          is_expected.not_to be_valid
        end
      end

      context '15時より後では' do
        subject { build(:working_day, :saturday_after_hour) }
        it 'バリデーションエラーになること' do
          is_expected.not_to be_valid
        end
      end
    end

    context '日曜日の時' do
      subject { build(:working_day, :sunday) }
      it 'バリデーションエラーになること' do
        is_expected.not_to be_valid
      end
    end
  end

  describe '#destroy' do
    let(:working_day) { create(:working_day, :with_booking) }
    before do
      working_day
    end
    it 'bookingも削除されること' do
      expect{ working_day.destroy }.to change{ Booking.all.size }.from(1).to(0)
    end
  end
end
