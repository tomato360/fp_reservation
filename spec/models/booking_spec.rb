# == Schema Information
#
# Table name: bookings
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  working_day_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'rails_helper'

RSpec.describe Booking, type: :model do
  describe '#create' do
    let(:booking) { create(:booking) }
    it "保存されること" do
      expect{ booking }.to change{ Booking.all.size }.by(1)
    end
  end
end
