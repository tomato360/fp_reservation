require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe 'GET #new' do
    it 'newテンプレートが使われる' do
      get :new
      is_expected.to render_template(:new)
    end
  end

  describe 'POST #create' do
    let(:user) { post :create, user: attributes_for(:user) }
    context '入力漏れがないとき' do
      it do
        expect{ user }.to change{ User.all.size }.by(1)
      end
    end
  end
end
