Rails.application.routes.draw do
  root to: 'default#index'

  resource :sessions, only: [:new, :create, :destroy]

  get 'users', controller: :users, action: :new
  resource :user, only: [:show, :destroy]
  resources :users do
    get 'complete', to: :complete
  end

  resources :bookings, only: :index

  resources :working_days, except: :show do
    resources :bookings, only: [:create, :destroy]
  end
end
